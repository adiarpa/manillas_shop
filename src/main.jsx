import React from 'react'
import ReactDOM from 'react-dom/client'
//import App from './App.jsx'
import './index.css'
import ManillaForm from './components/ManillasForm'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <ManillaForm />
  </React.StrictMode>,
)
