import React, {useState, useEffect} from 'react'
import { db } from '../firebase'
import { collection, doc, addDoc, onSnapshot, deleteDoc, updateDoc } from 'firebase/firestore'

const ManillaForm = () => {

  const [materialsList, setMaterialsList] = useState([])      // Material
  const [saidList, setSaidList] = useState([])                // Dijes
  const [typeList, setTypeList] = useState([])                // Tipo
  const [quantity, setQuantity] = useState(0);                // Cantidad
  const [handlesList, setHandlesList] = useState([])          // Manillas
  const [value, setValue] = useState("0");                    // Valor Unitario


  useEffect(()=>{
    const getHandlesList = async() =>{  // Se obtienen los datos de las manillas desde la DB
        try{
            await onSnapshot(collection(db, 'salesmanillas'), (query) =>{
                setHandlesList(query.docs.map((doc)=>({...doc.data(), id:doc.id})))
                console.log(handlesList)
            })
        }catch(error){
            console.log(error)
        }
    }
    getHandlesList();
  }, [])

  const updateValue = () =>{
    const material = document.getElementById("inputGroupSelect01").value
    const said = document.getElementById("inputGroupSelect02").value
    const type = document.getElementById("inputGroupSelect03").value
    const currency = document.getElementById("inputGroupSelect04").value
    const quantity = document.getElementById("quantity").value


    console.log(handlesList)

    if(material != "Seleccione." && said != "Seleccione." && type != "Seleccione." && currency!="Seleccione." && quantity !=''){
      for(let element of handlesList){
        if(element.material==material && element.dije==said && element.tipo==type){
          if(currency=="peso"){
            setValue((element.valorUnitario*quantity).toLocaleString("en")+" COP")
            break
          }
          if(currency=="dolar"){
            setValue((element.valorUnitario*quantity*5000).toLocaleString("en") +" Dólares")
            break
          }
        }else{
            setValue("0")
        }
    }   
    }
    console.log(value)
  }

  const quantityChangeEvent = (event) => {
    setQuantity(parseInt(event.target.value));
  };

  const quantityIncrement = () => {
    setQuantity(quantity + 1);
  };

  const quantityDecrement = () => {
    if (quantity > 0) {
      setQuantity(quantity - 1);
    }
  };

  return (
    <div class="container">

        <form>
          <div>
              <div>
                <br/>
                <hr/>
                <h1 className='text-center'>Bienvenidos a</h1>
                <h1 className='text-center'>XYZ - Manillas</h1>
                <hr/>
                <h4 className="text-center">El valor de su manilla puede variar según la siguiente tabla</h4>
                <table className='table'>
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">Material</th>
                        <th scope="col">Dije</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">Valor Unitario</th>
                        </tr>
                    </thead>
                    <tbody>
                        {  
                          handlesList.map(item =>(
                            <tr key={item.id}>                                    
                                <td>{item.material}</td>
                                <td>{item.dije}</td>
                                <td>{item.tipo}</td>
                                <td> $ {item.valorUnitario}</td>                                    
                            </tr>
                          ))   
                        }
                    </tbody>
                </table>
              </div>
            </div>

            <div class="row">
              <div className='col-2'>
                  <div class="input-group mb-3">
                      <div class="input-group-prepend">
                          <label for="inputGroupSelect01">Material</label>
                      </div>
                      <select class="custom-select" id="inputGroupSelect01" onChange={()=>updateValue()}>
                          <option selected>Seleccione.</option>
                          <option value="Cuero">Cuero</option>
                          <option value="Cuerda">Cuerda</option>
                      </select>
                  </div>
              </div>
              <div className='col-2'>
                  <div class="input-group mb-3">
                      <div class="input-group-prepend">
                          <label for="inputGroupSelect02">Dije</label>
                      </div>
                      <select class="custom-select" id="inputGroupSelect02" onChange={()=>updateValue()}>
                          <option selected>Seleccione.</option>
                          <option value="Martillo">Martillo</option>
                          <option value="Ancla">Ancla</option>
                      </select>
                  </div>
               </div>

                <div className='col-2'>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label for="inputGroupSelect03">Tipo</label>
                        </div>
                        <select class="custom-select" id="inputGroupSelect03" onChange={()=>updateValue()}>
                        <option selected>Seleccione.</option>
                            <option value="Oro">Oro</option>
                            <option value="Oro Rosado">Oro Rosado</option>
                            <option value="Plata">Plata</option>
                            <option value="Niquel">Niquel</option>
                        </select>
                    </div>
                </div>
                <div className='col-2'>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label for="inputGroupSelect04">Moneda</label>
                        </div>
                        <select class="custom-select" id="inputGroupSelect04" onChange={()=>updateValue()}>
                        <option selected>Seleccione.</option>
                            <option value="dolar">Dolar</option>
                            <option value="peso">Pesos</option>
                        </select>
                    </div>
                </div>       
              </div>

            <div className='col-2'>
                <div class="input-group mb-3">
                    <div>
                        <span id="basic-addon1">Cantidad</span>
                    </div>
                    <input type="number" id='quantity' onChange={()=>updateValue()}/>
                </div>
            </div>

              <div className='valor'>
                <span>Valor: {value} </span>
             </div>              

             <br/>
             <footer>
                <h4>AUTOR: Ing. Adiel Arguelles Padilla</h4>
             </footer>

          </form>
    </div>
    
  );
};

export default ManillaForm;