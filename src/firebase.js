import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import {getFirestore} from 'firebase/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyAlzW2u9Smi7O4yllvHOKY0_z4lXTyO1KQ",
  authDomain: "manillashopdb.firebaseapp.com",
  projectId: "manillashopdb",
  storageBucket: "manillashopdb.appspot.com",
  messagingSenderId: "816241775785",
  appId: "1:816241775785:web:7346cd255833161dfac5b8",
  measurementId: "G-H10DYV54RB"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app)
export {db}